package com.oauth.repository.auth;


import org.springframework.data.jpa.repository.JpaRepository;

import com.oauth.model.auth.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface UserRepository extends JpaRepository<User, String> {

  @Transactional(readOnly = true)
  boolean existsByUsername(String username);

  @Transactional(readOnly = true)
  long countByCompanyCode(String companyCode);

  @Transactional(readOnly = true)
  User findByUsername(String username);

  @Transactional(readOnly = true)
  User findByUsernameAndCompanyCode(String username, String companyCode);

  @Transactional(readOnly = true)
  @Query("SELECT m FROM User m WHERE m.id = :code ")
  User findByCode(@Param("code") String code);

  @Transactional(readOnly = true)
  @Query("SELECT m FROM User m WHERE "
          + "    m.code LIKE %:column% "
          + " OR m.username LIKE %:column% "
          + " OR m.email LIKE %:column% "
          + " OR m.activeStatus = :status "
          + " ")
  Page<User> findAllBySearch(@Param("column") String column, @Param("status") boolean status, Pageable pageable);

  @Transactional(readOnly = false)
  void deleteByCode(String code);

  @Transactional(readOnly = false)
  User save(User model);

}
