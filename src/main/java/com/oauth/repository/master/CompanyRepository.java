package com.oauth.repository.master;

import com.oauth.model.master.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface CompanyRepository extends JpaRepository<Company, String> {

  @Transactional(readOnly = true)
  @Query("SELECT m FROM Company m WHERE m.code = :code ")
  Company findByCode(@Param("code") String code);

  @Transactional(readOnly = true)
  @Query("SELECT m FROM Company m WHERE "
          + "    m.code LIKE %:column% "
          + " OR m.name LIKE %:column% "
          + " OR m.activeStatus = :status "
          + " ")
  Page<Company> findAllBySearch(@Param("column") String column, @Param("status") boolean status, Pageable pageable);

  @Transactional(readOnly = false)
  void deleteByCode(String code);
  
  @Transactional(readOnly = false)
  Company save(Company model);
}
