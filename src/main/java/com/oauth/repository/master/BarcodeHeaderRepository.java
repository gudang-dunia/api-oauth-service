package com.oauth.repository.master;

import com.oauth.model.master.BarcodeHeader;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface BarcodeHeaderRepository extends JpaRepository<BarcodeHeader, String> {
  
  @Transactional(readOnly = true)
  @Query("SELECT m FROM BarcodeHeader m WHERE m.code = :code ")
  BarcodeHeader findByCode(@Param("code") String code);

  @Transactional(readOnly = true)
  @Query("SELECT m FROM BarcodeHeader m WHERE "
          + "    m.code LIKE %:code% "
          + " ")
  List<BarcodeHeader> findAllBySearch(@Param("code") String code);

  @Transactional(readOnly = true)
  @Query("SELECT m FROM BarcodeHeader m WHERE "
          + "    m.code LIKE %:column% "
          + "    OR DATE(m.barcodeDate) BETWEEN DATE(:barcodeStart) AND DATE(:barcodeEnd) "
          + " ")
  Page<BarcodeHeader> pagesAllBySearch(@Param("column") String column, @Param("barcodeStart") String barcodeStart, @Param("barcodeEnd") String barcodeEnd, Pageable pageable);

  @Transactional(readOnly = false)
  void deleteByCode(String code);
  
  @Transactional(readOnly = false)
  BarcodeHeader save(BarcodeHeader model);
}
