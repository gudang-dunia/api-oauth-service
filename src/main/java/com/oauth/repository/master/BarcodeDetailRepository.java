package com.oauth.repository.master;

import com.oauth.model.master.BarcodeDetail;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface BarcodeDetailRepository extends JpaRepository<BarcodeDetail, String> {
  
  @Transactional(readOnly = true)
  @Query("SELECT m FROM BarcodeDetail m WHERE m.code = :code ")
  BarcodeDetail findByCode(@Param("code") String code);

  @Transactional(readOnly = true)
  @Query("SELECT m FROM BarcodeDetail m WHERE "
          + "    m.headerCode LIKE %:code% "
          + " ")
  List<BarcodeDetail> findAllBySearch(@Param("code") String code);

  @Transactional(readOnly = true)
  @Query("SELECT m FROM BarcodeDetail m WHERE "
          + "    m.headerCode LIKE %:column% "
          + " ")
  Page<BarcodeDetail> pagesAllBySearch(@Param("column") String column, Pageable pageable);
  
  @Transactional(readOnly = false)
  void deleteByHeaderCode(String code);
  
  @Transactional(readOnly = false)
  BarcodeDetail save(BarcodeDetail model);
}
