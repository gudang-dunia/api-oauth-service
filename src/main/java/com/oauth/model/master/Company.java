/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oauth.model.master;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.oauth.common.BaseModel;
import com.poiji.annotation.ExcelCell;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

/**
 *
 * @author imamsolikhin
 */
@Data
@Entity
@Table(name = "mst_company")
public class Company implements Serializable {

  @Id
  @ExcelCell(0)
  @Column(name = "id")
  private String code = "";

  @ExcelCell(1)
  @Column(name = "Name")
  private String name = "";

  @ExcelCell(2)
  @Column(name = "img_path")
  private String logoPath = "";

  @ExcelCell(3)
  @Column(name = "address")
  private String address = "";

  @ExcelCell(6)
  @Column(name = "phone")
  private String phone = "";

  @ExcelCell(8)
  @Column(name = "fax")
  private String fax = "";

  @ExcelCell(26)
  @Column(name = "status", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

  @JsonIgnore
  @CreatedBy
  @Column(name = "created_by", updatable = false)
  private String createdBy = "";

  @JsonIgnore
  @CreatedDate
  @Column(name = "created_at", updatable = false)
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date createdDate;

  @JsonIgnore
  @LastModifiedBy
  @Column(name = "updated_by", updatable = true)
  private String updatedBy = "";

  @JsonIgnore
  @LastModifiedDate
  @Column(name = "updated_at", updatable = true)
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date updatedDate;
}
