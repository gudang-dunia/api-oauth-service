package com.oauth.model.master;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.oauth.common.BaseModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

@Data
@Entity
@Table(name = "mst_item")
public class Item implements Serializable {

  @Id
  @Column(name = "id")
  private String code = "";

  @ApiModelProperty(hidden = true)
  @Column(name = "name")
  private String name = "";

  @ApiModelProperty(hidden = true)
  @Column(name = "company_id")
  private String companyCode = null;
  @ApiModelProperty(hidden = true)
  
  @Column(name = "remark")
  private String remark = null;

  @ApiModelProperty(hidden = true)
  @Column(name = "volume")
  private BigDecimal volume = new BigDecimal("0.00");

  @ApiModelProperty(hidden = true)
  @Column(name = "basedPrice")
  private BigDecimal basedPrice = new BigDecimal("0.00");

  @Column(name = "normalPrice")
  private BigDecimal normalPrice = new BigDecimal("0.00");

  @Column(name = "status", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = false;

  @JsonIgnore
  @CreatedBy
  @Column(name = "created_by", updatable = false)
  private String createdBy = "";

  @JsonIgnore
  @CreatedDate
  @Column(name = "created_at", updatable = false)
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date createdDate;

  @JsonIgnore
  @LastModifiedBy
  @Column(name = "updated_by", updatable = true)
  private String updatedBy = "";

  @JsonIgnore
  @LastModifiedDate
  @Column(name = "updated_at", updatable = true)
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date updatedDate;
}
