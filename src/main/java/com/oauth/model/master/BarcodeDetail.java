package com.oauth.model.master;

import com.oauth.common.BaseModel;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import lombok.Data;

@Data
@Entity
@Table(name = "mst_barcode_detail")
public class BarcodeDetail extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code", unique = true, nullable = true, insertable = true, updatable = true)
  private String code = "";

  @Column(name = "headerCode")
  private String headerCode = null;
  
  @Column(name = "barcodeDate", updatable = false)
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date barcodeDate;

  @Column(name = "keyCode")
  private String keyCode = null;

  @Column(name = "itemCode")
  private String itemCode = null;

  @Column(name = "companyCode")
  private String companyCode = null;

  @Column(name = "pathUrl")
  private String pathUrl = "";

  @Column(name = "pathLogo")
  private String pathLogo = "";

  @Column(name = "pathImage")
  private String pathImage = "";

  @Column(name = "barcodeStatus")
  private String barcodeStatus = "";
}
