package com.oauth.model.master;

import com.oauth.common.BaseModel;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import lombok.Data;

@Data
@Entity
@Table(name = "mst_barcode_header")
public class BarcodeHeader extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code", unique = true, nullable = true, insertable = true, updatable = true)
  private String code = "";

  @Column(name = "Name")
  private String name = null;

  @Column(name = "CompanyCode")
  private String CompanyCode = null;

  @Column(name = "barcodeDate", updatable = false)
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date barcodeDate;
  
  @Column(name = "Description")
  private String description = null;
}
