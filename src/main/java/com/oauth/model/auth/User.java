package com.oauth.model.auth;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import org.hibernate.annotations.ColumnDefault;

@Entity
@Table(name = "sys_user")
public class User implements Serializable {

  @Id
  @Column(name = "code")
  private String code;

  @Column(name = "companyCode", unique = true, nullable = false)
  private String companyCode;

  @Size(min = 4, max = 255, message = "Minimum username length: 4 characters")
  @Column(name = "username", unique = true, nullable = false)
  private String username;

  @Column(name = "useremail", unique = true, nullable = false)
  private String email;

  @Size(min = 8, message = "Minimum password length: 8 characters")
  @Column(name = "password")
  private String password;

  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  @ColumnDefault("1")
  private boolean activeStatus = true;

  @ElementCollection(fetch = FetchType.EAGER)
  @Column(name = "roleCode")
  List<Role> roles;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public boolean isActiveStatus() {
    return activeStatus;
  }

  public void setActiveStatus(boolean activeStatus) {
    this.activeStatus = activeStatus;
  }

  public List<Role> getRoles() {
    return roles;
  }

  public void setRoles(List<Role> roles) {
    this.roles = roles;
  }
}
