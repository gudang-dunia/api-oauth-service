package com.oauth.dto;

import lombok.Data;

@Data
public class BarcodeStatuDTO {
  private String id;
  private String produk;
  private String quantity;
  private String status;
  private String description;
  private String screet;
  private String logo;
}
