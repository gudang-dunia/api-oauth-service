package com.oauth.dto;

import lombok.Data;
import org.springframework.data.domain.Pageable;

@Data
public class Searching {

  private String code;
  private String name;
  private boolean status = true;
  private Pageable pageable;
}
