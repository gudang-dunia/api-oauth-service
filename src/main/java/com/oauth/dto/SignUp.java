package com.oauth.dto;

import java.util.List;

import com.oauth.model.auth.Role;
import lombok.Data;

@Data
public class SignUp {
  
  private String companycode;
  private String username;
  private String email;
  private String password;
  List<Role> roles;
}
