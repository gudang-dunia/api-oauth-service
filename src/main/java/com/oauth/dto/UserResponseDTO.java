package com.oauth.dto;

import java.util.List;
import com.oauth.model.auth.Role;
import lombok.Data;

@Data
public class UserResponseDTO {

  private Integer code;
  private String username;
  private String useremail;
  private String email;
  List<Role> roles;
}
