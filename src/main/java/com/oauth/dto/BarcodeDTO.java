package com.oauth.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BarcodeDTO {

  @ApiModelProperty(value="url_link", example = "http://check-produk.herbalputihindonesia.com/") 
  private String url_link = "http://check-produk.herbalputihindonesia.com/";
  
  @ApiModelProperty(value="url_logo", example = "http://check-produk.herbalputihindonesia.com/assets/icon/bpom.png") 
  private String url_logo = "http://check-produk.herbalputihindonesia.com/assets/icon/bpom.png";
  
  @ApiModelProperty(value="company_code", example = "HPI") 
  private String company_code;
  
  @ApiModelProperty(value="item_code", example = "MDH") 
  private String item_code;
  
  @ApiModelProperty(value="header_code", example = "HPIMDH211201") 
  private String header_code;
  
  @ApiModelProperty(value="product_start", example = "1") 
  private int product_start;
  
  @ApiModelProperty(value="product_end", example = "10") 
  private int product_end;
  
  @ApiModelProperty(value="barcode_date", example = "2021-12-01") 
  private String barcode_date;
  
  @ApiModelProperty(value="type_barcode", example = "978020137962") 
  private String type_barcode = "978020137962";
}
