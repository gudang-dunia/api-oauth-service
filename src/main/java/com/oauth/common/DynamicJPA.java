/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oauth.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import lombok.Data;

/**
 *
 * @author imamsolikhin
 */
@Data
public class DynamicJPA {

  public static String company = "";
  public static String perfix = "";
  public static int length = 1;
  public static String datePattern = "yyMMdd";

  public static String generadeCode(Long numb) {
    DateFormat df = new SimpleDateFormat(datePattern);
    String perfixDate = df.format(new Date());

    String combine = company + perfixDate + perfix;
    return combine + String.format("%0" + length + "d", numb);
  }
}
