/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oauth.common;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 *
 * @author imamsolikhin
 */
public class MyZip {

  public static class MemoryFile {

    public String fileName;
    public byte[] contents;
  }

  public byte[] createZipByteArray(List<MemoryFile> memoryFiles) throws IOException {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    ZipOutputStream zipOutputStream = new ZipOutputStream(byteArrayOutputStream);
    try {
      for (MemoryFile memoryFile : memoryFiles) {
        ZipEntry zipEntry = new ZipEntry(memoryFile.fileName);
        zipOutputStream.putNextEntry(zipEntry);
        zipOutputStream.write(memoryFile.contents);
        zipOutputStream.closeEntry();
      }
    } finally {
      zipOutputStream.close();
    }
    return byteArrayOutputStream.toByteArray();
  }

}
