package com.oauth.common;

public class PaginatedResults {

  private Object message = "";
  private Object data = "";
  private long currentData = 0;
  private long totalData = 0;
  private long totalPage = 0;

  public PaginatedResults(Object data) {
    this.data = data;
  }

  public PaginatedResults(Object data, Object message) {
    this.data = data;
  }

  public PaginatedResults(Object data, Object message, long currentData, long totalData, long totalPage) {
    this.message = message;
    this.data = data;
    this.currentData = currentData;
    this.totalData = totalData;
    this.totalPage = totalPage;
  }

  public Object getMessage() {
    return message;
  }

  public void setMessage(Object message) {
    this.message = message;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }

  public long getCurrentData() {
    return currentData;
  }

  public void setCurrentData(long currentData) {
    this.currentData = currentData;
  }

  public long getTotalData() {
    return totalData;
  }

  public void setTotalData(long totalData) {
    this.totalData = totalData;
  }

  public long getTotalPage() {
    return totalPage;
  }

  public void setTotalPage(long totalPage) {
    this.totalPage = totalPage;
  }

}
