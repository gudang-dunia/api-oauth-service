package com.oauth.service.auth;

import com.oauth.common.DynamicJPA;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.oauth.exception.CustomException;
import com.oauth.model.auth.User;
import com.oauth.repository.auth.UserRepository;
import com.oauth.security.JwtTokenProvider;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Service
public class UserService {

  @Autowired
  private UserRepository repo;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  private JwtTokenProvider jwtTokenProvider;

  @Autowired
  private AuthenticationManager authenticationManager;

  public String signin(String companyCode, String username, String password) {
    try {
      authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
      return jwtTokenProvider.createToken(username, repo.findByUsername(username).getRoles());
    } catch (AuthenticationException e) {
      throw new CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
    }
  }

  public String signup(User user) {
    if (!repo.existsByUsername(user.getUsername())) {
      String code = DynamicJPA.generadeCode(repo.countByCompanyCode(user.getCompanyCode()));
      user.setCode(code);
      user.setPassword(passwordEncoder.encode(user.getPassword()));
      repo.save(user);
      return jwtTokenProvider.createToken(user.getUsername(), user.getRoles());
    } else {
      throw new CustomException("Username is already in use", HttpStatus.UNPROCESSABLE_ENTITY);
    }
  }

  public User search(String username) {
    User user = repo.findByUsername(username);
    if (user == null) {
      throw new CustomException("The user doesn't exist", HttpStatus.NOT_FOUND);
    }
    return user;
  }

  public List<User> list() {
    List<User> users = repo.findAll();
    if (users == null) {
      throw new CustomException("The user doesn't exist", HttpStatus.NOT_FOUND);
    }
    return users;
  }

  public User whoami(HttpServletRequest req) {
    return repo.findByUsername(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(req)));
  }

  public String refresh(String username) {
    return jwtTokenProvider.createToken(username, repo.findByUsername(username).getRoles());
  }

  public List<User> list(String code, boolean status, Pageable pageable) {
    try {
      List<User> data = repo.findAllBySearch(code, status, pageable).getContent();
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.NOT_FOUND);
      }
      return data;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Page<User> pages(String code, boolean status, Pageable pageable) {
    try {
      Page<User> data = repo.findAllBySearch(code, status, pageable);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.NOT_FOUND);
      }
      return data;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public User data(String code) {
    try {
      User data = repo.findByCode(code);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.NOT_FOUND);
      }
      return data;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public User save(User model) {
    try {
      String code = DynamicJPA.generadeCode(repo.countByCompanyCode(model.getCompanyCode()));
      model.setCode(code);
      if (!repo.existsByUsername(model.getUsername())) {
        throw new CustomException("Data " + model.getUsername() + " already use!", HttpStatus.CONFLICT);
      }
      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public User update(User model) {
    try {
      User data = repo.findByCode(model.getCode());
      if (data == null) {
        throw new CustomException("Data " + model.getCode() + " can't update!", HttpStatus.CONFLICT);
      }
      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public User delete(String code) {
    try {
      User data = repo.findByCode(code);
      if (data == null) {
        throw new CustomException("Data " + code + " not found!", HttpStatus.FOUND);
      }
      repo.deleteByCode(code);
      return data;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

}
