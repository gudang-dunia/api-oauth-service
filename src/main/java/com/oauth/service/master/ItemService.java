package com.oauth.service.master;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.oauth.exception.CustomException;
import com.oauth.model.master.Item;
import com.oauth.repository.master.ItemRepository;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Service
public class ItemService {

  @Autowired
  private ItemRepository repo;

  public List<Item> list(String code, boolean status, Pageable pageable) {
    try {
      List<Item> data = repo.findAllBySearch(code, status, pageable).getContent();
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.NOT_FOUND);
      }
      return data;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
  
  public Page<Item> pages(String code, boolean status, Pageable pageable) {
    try {
      Page<Item> data = repo.findAllBySearch(code, status, pageable);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.NOT_FOUND);
      }
      return data;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Item data(String code) {
    try {
      Item data = repo.findByCode(code);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.NOT_FOUND);
      }
      return data;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Item save(Item model) {
    try {
      Item data = repo.findByCode(model.getCode());
      if (data != null) {
        throw new CustomException("Data " + model.getCode() + " already use!", HttpStatus.CONFLICT);
      }
      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Item update(Item model) {
    try {
      Item data = repo.findByCode(model.getCode());
      if (data == null) {
        throw new CustomException("Data " + model.getCode() + " can't update!", HttpStatus.CONFLICT);
      }
      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Item delete(String code) {
    try {
      Item data = repo.findByCode(code);
      if (data == null) {
        throw new CustomException("Data " + code + " not found!", HttpStatus.FOUND);
      }
      repo.deleteByCode(code);
      return data;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
