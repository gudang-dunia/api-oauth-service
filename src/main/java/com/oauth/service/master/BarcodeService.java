package com.oauth.service.master;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.oauth.exception.CustomException;
import com.oauth.model.master.BarcodeDetail;
import com.oauth.model.master.BarcodeHeader;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.oauth.repository.master.BarcodeDetailRepository;
import com.oauth.repository.master.BarcodeHeaderRepository;

@Service
public class BarcodeService extends Thread {

  @Autowired
  private BarcodeDetailRepository repo;

  @Autowired
  private BarcodeHeaderRepository repoHeader;

  public List<BarcodeDetail> list(String code) {
    try {
      List<BarcodeDetail> data = repo.findAllBySearch(code);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.NOT_FOUND);
      }
      return data;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Page<BarcodeDetail> pages(String code, Pageable pageable) {
    try {
      Page<BarcodeDetail> data = repo.pagesAllBySearch(code, pageable);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.NOT_FOUND);
      }
      return data;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Page<BarcodeHeader> pagesHeader(String column, String barcode_start, String barcode_end, Pageable pageable) {
    try {
      Page<BarcodeHeader> data = repoHeader.pagesAllBySearch(column, barcode_start, barcode_end, pageable);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.NOT_FOUND);
      }
      return data;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public BarcodeDetail data(String code) {
    try {
      BarcodeDetail data = repo.findByCode(code);
      return data;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public BarcodeDetail saveDetail(BarcodeDetail model) {
    try {
      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public BarcodeHeader saveHeader(BarcodeHeader model) {
    try {
      BarcodeDetail data = repo.findByCode(model.getCode());
      if (data != null) {
        throw new CustomException("Data " + model.getCode() + " already use!", HttpStatus.CONFLICT);
      }
      return repoHeader.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
