package com.oauth.service.master;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.oauth.exception.CustomException;
import com.oauth.model.master.Company;
import com.oauth.repository.master.CompanyRepository;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Service
public class CompanyService {

  @Autowired
  private CompanyRepository repo;

  public List<Company> list(String code, boolean status, Pageable pageable) {
    try {
      List<Company> data = repo.findAllBySearch(code, status, pageable).getContent();
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.NOT_FOUND);
      }
      return data;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
  
  public Page<Company> pages(String code, boolean status, Pageable pageable) {
    try {
      Page<Company> data = repo.findAllBySearch(code, status, pageable);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.NOT_FOUND);
      }
      return data;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Company data(String code) {
    try {
      Company data = repo.findByCode(code);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.NOT_FOUND);
      }
      return data;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Company save(Company model) {
    try {
      Company data = repo.findByCode(model.getCode());
      if (data != null) {
        throw new CustomException("Data " + model.getCode() + " already use!", HttpStatus.CONFLICT);
      }
      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Company update(Company model) {
    try {
      Company data = repo.findByCode(model.getCode());
      if (data == null) {
        throw new CustomException("Data " + model.getCode() + " can't update!", HttpStatus.CONFLICT);
      }
      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Company delete(String code) {
    try {
      Company data = repo.findByCode(code);
      if (data == null) {
        throw new CustomException("Data " + code + " not found!", HttpStatus.FOUND);
      }
      repo.deleteByCode(code);
      return data;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
