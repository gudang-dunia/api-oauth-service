package com.oauth.controller.auth;

import com.oauth.common.BaseProgress;
import com.oauth.common.PaginatedResults;
import com.oauth.common.ResponsBody;
import com.oauth.dto.Searching;
import com.oauth.exception.CustomException;
import com.oauth.model.auth.User;
import com.oauth.service.auth.UserService;
import com.poiji.bind.Poiji;
import com.poiji.exception.PoijiExcelType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/master/user")
@Api(tags = "Back Office Security User")
public class UserController extends BaseProgress {

  @Autowired
  private UserService service;

  @Autowired
  private ApplicationEventPublisher eventPublisher;

  @ApiOperation(value = "${comments.search-code}", authorizations = {
    @Authorization(value = "Authorization")})
  @GetMapping(value = "/{code}")
  public HttpEntity<Object> data(@PathVariable(required = false) String code) {
    try {
      User model = service.data(code);
      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  private Sort.Direction getSortDirection(String direction) {
    if (direction.equals("asc")) {
      return Sort.Direction.ASC;
    } else if (direction.equals("desc")) {
      return Sort.Direction.DESC;
    }

    return Sort.Direction.ASC;
  }

  @ApiOperation(value = "${comments.search-list}", authorizations = {
    @Authorization(value = "Authorization")})
  @GetMapping(value = "/search")
  public HttpEntity<Object> list(
          @RequestParam(required = false, defaultValue = " ") String column,
          @RequestParam(defaultValue = "true") boolean status,
          @RequestParam(defaultValue = "0") int page,
          @RequestParam(defaultValue = "10") int size,
          @RequestParam(defaultValue = "code,ASC") String[] sort) {
    try {
      List<Order> orders = new ArrayList<Order>();

      if (sort[0].contains(",")) {
        // will sort more than 2 fields
        // sortOrder="field, direction"
        for (String sortOrder : sort) {
          String[] _sort = sortOrder.split(",");
          orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
        }
      } else {
        // sort=[field, direction]
        orders.add(new Order(getSortDirection(sort[1]), sort[0]));
      }

      Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));
      Page<User> pages = service.pages(column, status, pagingSort);

      return new HttpEntity<>(new PaginatedResults(pages.getContent(), "Loading data success", pages.getNumber(), pages.getTotalElements(), pages.getTotalPages()));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @ApiOperation(value = "${comments.save}", authorizations = {
    @Authorization(value = "Authorization")})
  @PostMapping(value = "/save")
  public HttpEntity<Object> save(@ApiParam("Save master") @RequestBody User model) {
    try {
      User data = service.save(model);
      return new HttpEntity<>(new ResponsBody(data));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @ApiOperation(value = "${comments.update}", authorizations = {
    @Authorization(value = "Authorization")})
  @PostMapping(value = "/update")
  public HttpEntity<Object> update(@ApiParam("Update master") @RequestBody User model) {
    try {
      User data = service.update(model);
      return new HttpEntity<>(new ResponsBody(data));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @ApiOperation(value = "${comments.delete}", authorizations = {
    @Authorization(value = "Authorization")})
  @DeleteMapping(value = "/{code}")
  public HttpEntity<Object> delete(@PathVariable(required = false) String code) {
    try {
      User model = service.delete(code);
      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @PostMapping(value = "/import")
  public HttpEntity<Object> imports(@RequestParam MultipartFile file) {
    try {
      List<User> model = Poiji.fromExcel(file.getInputStream(), PoijiExcelType.XLS, User.class);
      for (User user : model) {
        service.save(user);
      }
      return new HttpEntity<>(new ResponsBody(model));
    } catch (IOException e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @PostMapping(value = "/export/xls")
  public HttpEntity<ByteArrayResource> exportXlsx(@ApiParam("Searching master") @RequestBody Searching dto) {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    XSSFWorkbook workbook = new XSSFWorkbook();
    try {
//      List<User> list = service.list(dto.getCode(), dto.getName(), dto.isStatus());

      HttpHeaders header = new HttpHeaders();
      header.setContentType(new MediaType("application", "force-download"));
      header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=export-user.xlsx");
      workbook.write(stream);
      workbook.close();

      return new ResponseEntity<>(new ByteArrayResource(stream.toByteArray()), header, HttpStatus.CREATED);
    } catch (IOException e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @PostMapping(value = "/export/pdf")
  public HttpEntity<ByteArrayResource> exportPdf(@ApiParam("Searching master") @RequestBody Searching dto) {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    try {
//      List<User> list = service.list(dto.getCode(), dto.getName(), dto.isStatus());

      HttpHeaders header = new HttpHeaders();
      header.setContentType(new MediaType("application", "force-download"));
      header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=export-user.pdf");

      return new ResponseEntity<>(new ByteArrayResource(stream.toByteArray()), header, HttpStatus.CREATED);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }
}
