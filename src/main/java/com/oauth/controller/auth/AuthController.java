package com.oauth.controller.auth;

import com.oauth.common.BaseProgress;
import com.oauth.dto.SignIn;
import com.oauth.dto.SignUp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import com.oauth.dto.UserResponseDTO;
import com.oauth.model.auth.User;
import com.oauth.service.auth.UserService;
import com.oauth.service.master.BarcodeService;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;

@RestController
@RequestMapping("oauth")
@Api(tags = "Auth")
public class AuthController extends BaseProgress {

  @Autowired
  private UserService userService;
  @Autowired
  private BarcodeService barcodeService;

  @Autowired
  private ModelMapper modelMapper;

  @Autowired
  ServletContext servletContext;

  @PostMapping("/signin")
  @ApiOperation(value = "${comments.signin}")
  @ApiResponses(value = {
    @ApiResponse(code = 400, message = "Something went wrong"),
    @ApiResponse(code = 422, message = "Invalid username/password supplied")})
  public String login(@ApiParam("Signin User") @RequestBody SignIn model) {
    return userService.signin(model.getCompanycode(), model.getUsername(), model.getPassword()
    );
  }

  @PostMapping("/signup")
  @ApiOperation(value = "${comments.signup}")
  @ApiResponses(value = {
    @ApiResponse(code = 400, message = "Something went wrong"),
    @ApiResponse(code = 403, message = "Access denied"),
    @ApiResponse(code = 422, message = "Username is already in use")})
  public String signup(@ApiParam("Signup User") @RequestBody SignUp user) {
    User model = new User();
    model.setCompanyCode(user.getCompanycode());
    model.setEmail(user.getEmail());
    model.setUsername(user.getUsername());
    model.setPassword(user.getPassword());
    model.setActiveStatus(true);
    return userService.signup(model);
  }

  @GetMapping(value = "/{username}")
  @ApiOperation(value = "${comments.search}", response = UserResponseDTO.class, authorizations = {
    @Authorization(value = "Authorization")})
  @ApiResponses(value = {
    @ApiResponse(code = 400, message = "Something went wrong"),
    @ApiResponse(code = 403, message = "Access denied"),
    @ApiResponse(code = 404, message = "The user doesn't exist"),
    @ApiResponse(code = 500, message = "Expired or invalid JWT token")})
  public UserResponseDTO search(@ApiParam("Username") @PathVariable String username) {
    return modelMapper.map(userService.search(username), UserResponseDTO.class);
  }

  @GetMapping(value = "/me")
  @ApiOperation(value = "${comments.me}", response = UserResponseDTO.class, authorizations = {
    @Authorization(value = "Authorization")})
  @ApiResponses(value = {
    @ApiResponse(code = 400, message = "Something went wrong"),
    @ApiResponse(code = 403, message = "Access denied"),
    @ApiResponse(code = 500, message = "Expired or invalid JWT token")})
  public UserResponseDTO whoami(HttpServletRequest req) {
    return modelMapper.map(userService.whoami(req), UserResponseDTO.class);
  }

  @GetMapping("/refresh")
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
  public String refresh(HttpServletRequest req) {
    return userService.refresh(req.getRemoteUser());
  }
}
