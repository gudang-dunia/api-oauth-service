package com.oauth.controller.master;

import com.oauth.common.BarcodeGenerator;
import com.oauth.common.EnumDataType;
import com.oauth.common.IOExcel;
import com.oauth.common.PaginatedResults;
import com.oauth.common.QrCodeGenerator;
import com.oauth.common.ResponsBody;
import com.oauth.dto.BarcodeDTO;
import com.oauth.dto.BarcodeStatuDTO;
import com.oauth.model.master.BarcodeDetail;
import com.oauth.model.master.BarcodeHeader;
import com.oauth.model.master.Item;
import com.oauth.service.master.BarcodeService;
import com.oauth.service.master.ItemService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

@CrossOrigin
@RestController
@RequestMapping("/master/barcode")
@Api(tags = "Back Office Master Barcode")
public class BarcodeController {

  @Autowired
  private BarcodeService service;

  @Autowired
  private ItemService itemService;

  private String[] descProduct = new String[]{
    "Produk yang kamu beli jika asli",
    "terdaftar di Badan Pengawas Obat",
    "dan Makanan (BPOM) baik itu produk",
    "lokal atau import",};

  @Async
  @ApiOperation(value = "${comments.generade}", authorizations = {
    @Authorization(value = "Authorization")})
  @PostMapping(value = "/generade-barcode", produces = MediaType.IMAGE_PNG_VALUE)
  public ResponseEntity<Object> generateBarcode(
          @RequestBody BarcodeDTO dto,
          @Autowired BarcodeGenerator barcodeGenerator) {

    try {
      String pattern = "yyMMdd";
      DateFormat df = new SimpleDateFormat(pattern);
      Date today = Calendar.getInstance().getTime();
      String todayAsString = df.format(today);

      String perfix = dto.getCompany_code() + dto.getItem_code() + todayAsString;
      String perfixStartEnd = perfix + String.format("%06d", dto.getProduct_start()) + String.format("%06d", dto.getProduct_end());

      BarcodeHeader modelHeader = new BarcodeHeader();
      modelHeader.setCode(perfixStartEnd);
      modelHeader.setName(perfix + " " + dto.getProduct_start() + " to " + dto.getProduct_end());
      modelHeader.setCompanyCode(dto.getCompany_code());
      modelHeader.setDescription("-");
      service.saveHeader(modelHeader);

      BarcodeDetail model = new BarcodeDetail();
      model.setBarcodeStatus("READY REGISTERED");
      model.setCode(perfix + String.format("%06d", dto.getProduct_start()));
      model.setHeaderCode(dto.getHeader_code());
      model.setItemCode(dto.getItem_code());
      model.setCompanyCode(dto.getCompany_code());
      service.saveDetail(model);

      switch (dto.getType_barcode()) {
        case "EAN13":
          // 978020137962
          return new ResponseEntity<>(barcodeGenerator.generateEAN13BarcodeImage(model.getCode()), OK);
        case "UPC":
          // 12345678901
          return new ResponseEntity<>(barcodeGenerator.generateUPCBarcodeImage(model.getCode()), OK);
        case "EAN128":
          // 0101234567890128TEC
          return new ResponseEntity<>(barcodeGenerator.generateEAN128BarCodeImage(model.getCode()), OK);
        case "CODE128":
          // any-string
          return new ResponseEntity<>(barcodeGenerator.generateCode128BarCodeImage(model.getCode()), OK);
        case "USPS":
          // 123456789
          return new ResponseEntity<>(barcodeGenerator.generateUSPSBarcodeImage(model.getCode()), OK);
        case "SCC14":
          return new ResponseEntity<>(barcodeGenerator.generateSCC14ShippingCodeBarcodeImage(model.getCode()), OK);
        case "CODE39":
          return new ResponseEntity<>(barcodeGenerator.generateCode39BarcodeImage(model.getCode()), OK);
        case "GTIN":
          return new ResponseEntity<>(barcodeGenerator.generateGlobalTradeItemNumberBarcodeImage(model.getCode()), OK);
        case "PDF417":
          return new ResponseEntity<>(barcodeGenerator.generatePDF417BarcodeImage(model.getCode()), OK);
        default:
          return null;
      }
    } catch (Exception ex) {
//      System.out.println(ex.getMessage());
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
    }
  }

  @Async
  @ApiOperation(value = "${comments.generade}", authorizations = {
    @Authorization(value = "Authorization")})
  @PostMapping(value = "/generade-qr")
  public HttpEntity<Object> generateQrCode(
          @RequestBody BarcodeDTO dto,
          @Autowired final QrCodeGenerator qrCodeGenerator) {
    try {
      if (dto.getUrl_logo() != null || !dto.getUrl_logo().equals("")) {
        QrCodeGenerator.LOGO = dto.getUrl_logo();
      }
      String pattern = "yyMMdd";
      DateFormat df = new SimpleDateFormat(pattern);
      Date dt = new SimpleDateFormat("yyyy-MM-dd").parse(dto.getBarcode_date());
      String todayAsString = df.format(dt);

      String perfix = dto.getCompany_code() + dto.getItem_code() + todayAsString;
      String perfixStartEnd = perfix + String.format("%06d", dto.getProduct_start()) + String.format("%06d", dto.getProduct_end());

      BarcodeHeader modelHeader = new BarcodeHeader();
      modelHeader.setCode(perfixStartEnd);
      modelHeader.setName(perfix + " " + dto.getProduct_start() + " to " + dto.getProduct_end());
      modelHeader.setCompanyCode(dto.getCompany_code());
      modelHeader.setBarcodeDate(dt);
      modelHeader.setDescription("-");
      service.saveHeader(modelHeader);

      Random rand = new Random();
      List<BarcodeDetail> modelList = new ArrayList<BarcodeDetail>();

      for (int i = dto.getProduct_start(); i <= dto.getProduct_end(); i++) {
        BarcodeDetail model = new BarcodeDetail();
        model.setBarcodeStatus("READY REGISTERED");
        model.setHeaderCode(modelHeader.getCode());
        model.setCode(perfix + String.format("%06d", i));
        model.setCompanyCode(dto.getCompany_code());
        model.setItemCode(dto.getItem_code());
        model.setKeyCode(String.format("%04d", rand.nextInt(10000)));
        model.setBarcodeDate(dt);

        String encodeCode = Base64.getEncoder().encodeToString(model.getCode().getBytes());
        model.setPathUrl(dto.getUrl_link() + encodeCode);
        model.setPathImage("http://103.163.139.199:9000/api-oauth-service/master/barcode/check/" + encodeCode);
        model.setPathLogo(dto.getUrl_logo());

        service.saveDetail(model);
        modelList.add(model);
      }
      return new HttpEntity<>(new ResponsBody(modelList, "Barcode created success"));

    } catch (Exception ex) {
//      System.out.println(ex.getMessage());
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
    }
  }

  @GetMapping(value = "/check/{product_id}", produces = MediaType.IMAGE_PNG_VALUE)
  public ResponseEntity<Object> checkQrCode(
          @PathVariable(required = true) String product_id,
          @Autowired final QrCodeGenerator qrCodeGenerator) {
    try {
      byte[] decodedBytes = Base64.getDecoder().decode(product_id);
      String decodedString = new String(decodedBytes);

      BarcodeDetail model = service.data(decodedString);
      if (model == null) {
        URL url = new URL("https://shop.unicornstore.in/assets/img/ProductNotFound.png");
        return new ResponseEntity<>(ImageIO.read(url), OK);
      }

//      QrCodeGenerator.LOGO = model.getPathLogo();
      return new ResponseEntity<>(qrCodeGenerator.generatePotrait(model.getPathUrl(), 300, 300, "[secret]"), OK);
//      return new ResponseEntity<>(qrCodeGenerator.generatePotrait(model.getPathUrl(), 300, 300, model.getKeyCode()), OK);
    } catch (Exception ex) {
//      System.out.println(ex.getMessage());
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
    }
  }

  @GetMapping(value = "/status/{product_id}")
  public HttpEntity<Object> statusQrCode(
          @PathVariable(required = true) String product_id) {
    BarcodeStatuDTO data = new BarcodeStatuDTO();
    try {
      if (product_id.equals("")) {
        return new HttpEntity<>(new ResponsBody("Not Valid"));
      }
      String decodedString = new String(Base64.getDecoder().decode(product_id));
      BarcodeDetail model = service.data(decodedString);
      if (model == null) {
        data.setId(decodedString);
        data.setStatus("QR NOT VALID");
        data.setScreet("QR NOT VALID");
        data.setProduk("QR NOT VALID");
        data.setQuantity("QR NOT VALID");
        data.setLogo("https://check-produk.herbalputihindonesia.com//assets/images/ProductNotFound.png");
        data.setDescription("Mohon Maaf Produk Anda Tidak Terdaftar.!!");
        return new HttpEntity<>(new ResponsBody(data));
      }
      
      Item item = itemService.data(model.getItemCode());
      data.setId(decodedString);
      data.setProduk(item.getName());
      data.setQuantity(item.getRemark());
      data.setStatus(model.getBarcodeStatus());
      data.setScreet(Base64.getEncoder().encodeToString(model.getKeyCode().getBytes()));
      data.setDescription("Product Anda Terdaftar, dan Terverifikasi oleh BPOM.");
      data.setLogo(model.getPathLogo());

      if (model.getBarcodeStatus().equals("REGISTERED")) {
        data.setDescription("Selamat, Produk sudah ter Verifikasi.!!");
      }

      return new HttpEntity<>(new ResponsBody(data));
    } catch (Exception ex) {
//      System.out.println(ex.getMessage());
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
    }
  }

  @Async
  @ApiOperation(value = "${comments.update}", authorizations = {
    @Authorization(value = "Authorization")})
  @GetMapping(value = "/registry/{product_id}/{screet_key}")
  public HttpEntity<Object> registQrCode(
          @PathVariable(required = true) String product_id,
          @PathVariable(required = true) String screet_key) {
    BarcodeStatuDTO data = new BarcodeStatuDTO();
    try {
      byte[] decodedBytes = Base64.getDecoder().decode(product_id);
      String decodedString = new String(decodedBytes);
      BarcodeDetail model = service.data(decodedString);
      if (model == null) {
        data.setId(decodedString);
        data.setStatus("QR NOT VALID");
        data.setScreet("KEY NOT FOUND");
        data.setDescription("Mohon Maaf Produk Anda Tidak Terdaftar.!!");
        return new HttpEntity<>(new ResponsBody(data));
      }
      if (!model.getKeyCode().equals(screet_key)) {
        data.setId(decodedString);
        data.setStatus(model.getBarcodeStatus());
        data.setScreet("KEY NOT VALID");
        data.setDescription("Mohon Maaf Produk Anda Tidak Terdaftar.!!");
        return new HttpEntity<>(new ResponsBody(data));
      }
      model.setBarcodeStatus("REGISTERED");
      service.saveDetail(model);

      data.setId(decodedString);
      data.setStatus(model.getBarcodeStatus());
      data.setScreet(Base64.getEncoder().encodeToString(model.getKeyCode().getBytes()));
      data.setDescription("Selamat, Produk sudah ter Verifikasi.!!");

      return new HttpEntity<>(new ResponsBody(data));
    } catch (Exception ex) {
//      System.out.println(ex.getMessage());
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
    }
  }

  private Sort.Direction getSortDirection(String direction) {
    if (direction.equals("asc")) {
      return Sort.Direction.ASC;
    } else if (direction.equals("desc")) {
      return Sort.Direction.DESC;
    }

    return Sort.Direction.ASC;
  }

  @ApiOperation(value = "${comments.zip}", authorizations = {
    @Authorization(value = "Authorization")})
    @GetMapping(value = "/export-zip/{header}")
  public void zipQrCode(
          HttpServletResponse response,
          @PathVariable(required = true) String header) {

    response.setContentType("application/octet-stream");
    response.setHeader("Content-Disposition", "attachment;filename=DOCQR-" + header + ".zip");
    response.setStatus(HttpServletResponse.SC_OK);

    List<BarcodeDetail> list = service.list(header);

    try (ZipOutputStream zippedOut = new ZipOutputStream(response.getOutputStream())) {
      for (BarcodeDetail model : list) {
//        System.out.print(model.getCode());
        QrCodeGenerator qrCodeGenerator = new QrCodeGenerator();
        QrCodeGenerator.LOGO = model.getPathLogo();
        BufferedImage qrImage = qrCodeGenerator.generatePotrait(model.getPathUrl(), 300, 300, model.getKeyCode());

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(qrImage, "png", baos);
        byte[] bytes = baos.toByteArray();

        ZipEntry zipEntry = new ZipEntry(model.getCode() + ".png");
        zipEntry.setSize(baos.size());
        zipEntry.setTime(System.currentTimeMillis());
        zippedOut.putNextEntry(zipEntry);
        zippedOut.write(bytes);
        zippedOut.closeEntry();
      }
      zippedOut.finish();

    } catch (Exception ex) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
    }
  }

  @ApiOperation(value = "${comments.export-excel}", authorizations = {
    @Authorization(value = "Authorization")})
  @GetMapping(value = "/export-excel/{header}")
  public void exportExcelList(
          HttpServletResponse response,
          @PathVariable(required = false) String header) throws ParseException {

    response.setContentType("application/octet-stream");
    response.setHeader("Content-Disposition", "attachment;filename=DOCQR-" + header + ".xlsx");
    response.setStatus(HttpServletResponse.SC_OK);

    List<BarcodeDetail> list = service.list(header);
    XSSFWorkbook wb = new XSSFWorkbook();
    IOExcel.exportUtils(wb, header);

    // Header
//      IOExcel.expCellValues(Temp, EnumDataType.ENUM_DataType.STRING, 0, 1, true);
//      IOExcel.expCellValues("Journal Recapitulation", EnumDataType.ENUM_DataType.STRING, 2, 1, true);
//      IOExcel.expCellValues("Printed By : " + user, EnumDataType.ENUM_DataType.STRING, 2, 13, true);
//      IOExcel.expCellValues("Periode :" + firstDateView, EnumDataType.ENUM_DataType.STRING, 4, 1, true);
//      IOExcel.expCellValues("Up To :" + lastDateView, EnumDataType.ENUM_DataType.STRING, 4, 3, true);
//      IOExcel.expCellValues("Printed Date : " + printedDate, EnumDataType.ENUM_DataType.STRING, 4, 13, true);
    int rowHeader = 1;
    IOExcel.cols = 0;
    IOExcel.expCellValues("No", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Company Code", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Item Code", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Group Code", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Product Code", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Screet Key", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("QR Status", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Path Image", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Path Logo", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Path Url", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);

    int no = 1;
    int row = 2;
    for (BarcodeDetail model : list) {
      IOExcel.cols = 0;
      IOExcel.expCellValues(Integer.toString(no++), EnumDataType.ENUM_DataType.NUMERIC, row, IOExcel.cols + 1);
      IOExcel.expCellValues(model.getCompanyCode(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(model.getItemCode(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(model.getHeaderCode(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(model.getCode(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(model.getKeyCode(), EnumDataType.ENUM_DataType.NUMERIC, row, IOExcel.cols + 1);
      IOExcel.expCellValues(model.getBarcodeStatus(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(model.getPathImage(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(model.getPathLogo(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(model.getPathUrl(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      row++;
    }

    try {
      response.getOutputStream().write(IOExcel.exportExcel());
      response.getOutputStream().close();
    } catch (Exception ex) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
    }
  }

  @ApiOperation(value = "${comments.search-list}", authorizations = {
    @Authorization(value = "Authorization")})
  @GetMapping(value = "/data-detail")
  public HttpEntity<Object> listQrCode(
          @RequestParam(required = false, defaultValue = " ") String header,
          @RequestParam(defaultValue = "true") boolean status,
          @RequestParam(defaultValue = "0") int page,
          @RequestParam(defaultValue = "10") int size,
          @RequestParam(defaultValue = "code,ASC") String[] sort
  ) {
    try {
      List<Sort.Order> orders = new ArrayList<Sort.Order>();

      if (sort[0].contains(",")) {
        // will sort more than 2 fields
        // sortOrder="field, direction"
        for (String sortOrder : sort) {
          String[] _sort = sortOrder.split(",");
          orders.add(new Sort.Order(getSortDirection(_sort[1]), _sort[0]));
        }
      } else {
        // sort=[field, direction]
        orders.add(new Sort.Order(getSortDirection(sort[1]), sort[0]));
      }

      Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));
      Page<BarcodeDetail> pages = service.pages(header, pagingSort);

      return new HttpEntity<>(new PaginatedResults(pages.getContent(), "Loading data success", pages.getNumber(), pages.getTotalElements(), pages.getTotalPages()));
    } catch (Exception ex) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
    }
  }

  @ApiOperation(value = "${comments.search-list}", authorizations = {
    @Authorization(value = "Authorization")})
  @GetMapping(value = "/data-header")
  public HttpEntity<Object> listHeaerQrCode(
          @RequestParam(required = false, defaultValue = " ") String column,
          @RequestParam(required = false, defaultValue = "2021-12-01") String barcode_start,
          @RequestParam(required = false, defaultValue = "2021-12-31") String barcode_end,
          @RequestParam(defaultValue = "true") boolean status,
          @RequestParam(defaultValue = "0") int page,
          @RequestParam(defaultValue = "10") int size,
          @RequestParam(defaultValue = "code,ASC") String[] sort
  ) {
    try {
      List<Sort.Order> orders = new ArrayList<Sort.Order>();

      if (sort[0].contains(",")) {
        for (String sortOrder : sort) {
          String[] _sort = sortOrder.split(",");
          orders.add(new Sort.Order(getSortDirection(_sort[1]), _sort[0]));
        }
      } else {
        orders.add(new Sort.Order(getSortDirection(sort[1]), sort[0]));
      }

      Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));
      Page<BarcodeHeader> pages = service.pagesHeader(column, barcode_start, barcode_end, pagingSort);

      return new HttpEntity<>(new PaginatedResults(pages.getContent(), "Loading data success", pages.getNumber(), pages.getTotalElements(), pages.getTotalPages()));
    } catch (Exception ex) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
    }
  }
}
