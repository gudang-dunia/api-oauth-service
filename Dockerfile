FROM tomcat:9.0.37-jdk11-adoptopenjdk-hotspot
COPY api-oauth-service.war /usr/local/tomcat/webapps/api.war

EXPOSE 8080
CMD ["catalina.sh", "run"]
