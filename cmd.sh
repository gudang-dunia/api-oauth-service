#!/bin/sh
docker build -t "class-core" .  || TRUE
docker rm $(docker stop $(docker ps -a -q --filter name="class-core" --format="{{.ID}}")) || TRUE
docker run -i -d -p 9191:8080 --name="class-core" -v $PWD:/app "class-core"
